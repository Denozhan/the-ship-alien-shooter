﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Chaser::Start()
extern void Chaser_Start_mBF0E7582ABA5CEBEE09A80E52BE2869DEA0EF272 (void);
// 0x00000002 System.Void Chaser::Update()
extern void Chaser_Update_m3242C4DC2FC49423CC5A74FA74D082BCFD4DD1BD (void);
// 0x00000003 System.Void Chaser::SetTarget(UnityEngine.Transform)
extern void Chaser_SetTarget_mDFD1CE91290EA77D3EF0BCC57F41FEC893CA555D (void);
// 0x00000004 System.Void Chaser::Attack()
extern void Chaser_Attack_mDB506A83B3D6283396537CD22F5A082F02A52EDC (void);
// 0x00000005 System.Void Chaser::OnDrawGizmosSelected()
extern void Chaser_OnDrawGizmosSelected_m94DAF6A6B20B90C0B4AE7762658F573FC24C1B67 (void);
// 0x00000006 System.Void Chaser::.ctor()
extern void Chaser__ctor_m990DFFA4112C0A2012C45A369DEA00CCB2D8C5C3 (void);
// 0x00000007 System.Void EnemyBehaviour::Start()
extern void EnemyBehaviour_Start_m3F9AC6A84CB077FDD09407112AE62BF3EA3130E1 (void);
// 0x00000008 System.Void EnemyBehaviour::Update()
extern void EnemyBehaviour_Update_m217ADFABABC91E6F24A3AA92B51843F4292E146C (void);
// 0x00000009 System.Void EnemyBehaviour::reduceHealth(System.Int32)
extern void EnemyBehaviour_reduceHealth_mAF544A146AD3E2309958558D39D293C7F235F76D (void);
// 0x0000000A System.Void EnemyBehaviour::.ctor()
extern void EnemyBehaviour__ctor_m64123744E6DEED46299C1DDC37F6D9F625A04556 (void);
// 0x0000000B System.Void FPCharacterController3D::Start()
extern void FPCharacterController3D_Start_mBACB9BDE34A7A2C7F5FC03699A8C570C3C84AB10 (void);
// 0x0000000C System.Void FPCharacterController3D::Update()
extern void FPCharacterController3D_Update_m9432A3D9BB9526AF736A6042F0A6AA00A1C0E090 (void);
// 0x0000000D System.Void FPCharacterController3D::.ctor()
extern void FPCharacterController3D__ctor_m8F5B5BB7460847852462EC539013455B3C99A3A3 (void);
// 0x0000000E System.Void GameManager::Start()
extern void GameManager_Start_m26461AEF27E44DB8FECCBC19D6C9E228B658BF8E (void);
// 0x0000000F System.Void GameManager::Update()
extern void GameManager_Update_mC9303BA7C3117BD861F49F8E36151CC52117E6C1 (void);
// 0x00000010 System.Void GameManager::reduceHealth()
extern void GameManager_reduceHealth_m8CB257A73E16A1D46EAB9C277187712475A4B5FF (void);
// 0x00000011 System.Void GameManager::earnPoints()
extern void GameManager_earnPoints_mC373DB2CBAFC24415EF5236A0432D0CF89B3D049 (void);
// 0x00000012 System.Void GameManager::gameOver()
extern void GameManager_gameOver_m0C5438CD9609B29FB90604BB3E3A45196FA4DBAF (void);
// 0x00000013 System.Collections.IEnumerator GameManager::loadSceneAgain()
extern void GameManager_loadSceneAgain_m3FE163CC04E7EAE594080175EB5CAD4CD4B1147E (void);
// 0x00000014 System.Void GameManager::.ctor()
extern void GameManager__ctor_mE8666F6D0CA9C31E16B719F79780DC4B0245B64D (void);
// 0x00000015 System.Void GameManager/<loadSceneAgain>d__16::.ctor(System.Int32)
extern void U3CloadSceneAgainU3Ed__16__ctor_m98409BEABFB6FEA644054300F989E56D3399AAB8 (void);
// 0x00000016 System.Void GameManager/<loadSceneAgain>d__16::System.IDisposable.Dispose()
extern void U3CloadSceneAgainU3Ed__16_System_IDisposable_Dispose_mEF81B8CA677E4757F09FD75053F6A84BA67FD5EB (void);
// 0x00000017 System.Boolean GameManager/<loadSceneAgain>d__16::MoveNext()
extern void U3CloadSceneAgainU3Ed__16_MoveNext_mDBDE5A1A47E38621A3C6B6E6256BA87A7DA62A8D (void);
// 0x00000018 System.Object GameManager/<loadSceneAgain>d__16::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CloadSceneAgainU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m66F5E55CD5A6DBBF3D633C476F0BF710F6F5FA79 (void);
// 0x00000019 System.Void GameManager/<loadSceneAgain>d__16::System.Collections.IEnumerator.Reset()
extern void U3CloadSceneAgainU3Ed__16_System_Collections_IEnumerator_Reset_m53B343D1A20609A4314E1A2E772CD046F86FF6F9 (void);
// 0x0000001A System.Object GameManager/<loadSceneAgain>d__16::System.Collections.IEnumerator.get_Current()
extern void U3CloadSceneAgainU3Ed__16_System_Collections_IEnumerator_get_Current_m886CFB2E619F5A992E6E0F64A51D4AC0C96238D3 (void);
// 0x0000001B System.Void MouseLook::Start()
extern void MouseLook_Start_m699B23D66C4F21B566C48A524BC40A828F5E3541 (void);
// 0x0000001C System.Void MouseLook::Update()
extern void MouseLook_Update_m3D49361C94E0433BB35499708EE783B4543D83D5 (void);
// 0x0000001D System.Void MouseLook::.ctor()
extern void MouseLook__ctor_mD12D8075DDEA2085341B59FF8BA9FB353613200B (void);
// 0x0000001E System.Void Shooter::Update()
extern void Shooter_Update_mF95D0E122742F65FF3236CC03DDE9CA17DDCB7E2 (void);
// 0x0000001F System.Void Shooter::.ctor()
extern void Shooter__ctor_m173C774BF5668018A0B10ACA4BB5A6DA3A6BAF2D (void);
// 0x00000020 System.Void Spawner::Start()
extern void Spawner_Start_m61D97BD980BD1B1877634A1E7626E47418D5D6D8 (void);
// 0x00000021 System.Collections.IEnumerator Spawner::Spawn()
extern void Spawner_Spawn_mDE0F1460D4AF60FCE476DA897B2E7EFC2CE7757F (void);
// 0x00000022 System.Void Spawner::.ctor()
extern void Spawner__ctor_m08E8D40AAA40F4329D8A95EEE2B2B6BE842CEB9C (void);
// 0x00000023 System.Void Spawner/<Spawn>d__5::.ctor(System.Int32)
extern void U3CSpawnU3Ed__5__ctor_m69706B0276989D786D59CE95A6F860DEE0B2363B (void);
// 0x00000024 System.Void Spawner/<Spawn>d__5::System.IDisposable.Dispose()
extern void U3CSpawnU3Ed__5_System_IDisposable_Dispose_m7F6FCA27C404BA2710C19B5AB653B907AD0DD24F (void);
// 0x00000025 System.Boolean Spawner/<Spawn>d__5::MoveNext()
extern void U3CSpawnU3Ed__5_MoveNext_mEFF08C630CFE924711A8775CA21A114B3625B77D (void);
// 0x00000026 System.Object Spawner/<Spawn>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSpawnU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m30C2C53935086363A737643AF6C711E1316492AE (void);
// 0x00000027 System.Void Spawner/<Spawn>d__5::System.Collections.IEnumerator.Reset()
extern void U3CSpawnU3Ed__5_System_Collections_IEnumerator_Reset_mC20553BB7E3124B38D5CB9842F0728EEDC820D3A (void);
// 0x00000028 System.Object Spawner/<Spawn>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CSpawnU3Ed__5_System_Collections_IEnumerator_get_Current_mEBD2A653118248414B8194740827EB232443D9A8 (void);
// 0x00000029 System.Collections.IEnumerator TextController::Start()
extern void TextController_Start_m280C87919FD642A897AAB86298E73E3055B1B0F5 (void);
// 0x0000002A System.Void TextController::Update()
extern void TextController_Update_m1E0250612056DC745E7ADBF3978B723C7A7854A3 (void);
// 0x0000002B System.Collections.IEnumerator TextController::EffectTypeWriter(System.String,UnityEngine.UI.Text)
extern void TextController_EffectTypeWriter_mB9B8D7C2507F94B53C226E009427E791BEA622C0 (void);
// 0x0000002C System.Collections.IEnumerator TextController::hideText()
extern void TextController_hideText_m8A47D7D23A43B14AB88D7228355EA52172D92768 (void);
// 0x0000002D System.Void TextController::.ctor()
extern void TextController__ctor_m62ECE2CB6D0A4F91F707E8FD0D9E288282EAF87D (void);
// 0x0000002E System.Void TextController/<Start>d__9::.ctor(System.Int32)
extern void U3CStartU3Ed__9__ctor_m9F7FFB42B5C398095311941B3DB36F77ED7E26D9 (void);
// 0x0000002F System.Void TextController/<Start>d__9::System.IDisposable.Dispose()
extern void U3CStartU3Ed__9_System_IDisposable_Dispose_m83DF870E50BD288E98A9623ABB0C09A2FF855695 (void);
// 0x00000030 System.Boolean TextController/<Start>d__9::MoveNext()
extern void U3CStartU3Ed__9_MoveNext_mA273D2180B707108C2F11738E042D717E1C1B733 (void);
// 0x00000031 System.Object TextController/<Start>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE86B6688501ABB91131C54BF7AC5634F60398069 (void);
// 0x00000032 System.Void TextController/<Start>d__9::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__9_System_Collections_IEnumerator_Reset_mDAE82A4B05CD3A0EF42E1A2AE6624C6929A35B37 (void);
// 0x00000033 System.Object TextController/<Start>d__9::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__9_System_Collections_IEnumerator_get_Current_m8928E6243C80FA73AD5CF468358680472CF9A755 (void);
// 0x00000034 System.Void TextController/<EffectTypeWriter>d__11::.ctor(System.Int32)
extern void U3CEffectTypeWriterU3Ed__11__ctor_mA232EFA112F6D682C597FB8ED6DA2894CFC3660A (void);
// 0x00000035 System.Void TextController/<EffectTypeWriter>d__11::System.IDisposable.Dispose()
extern void U3CEffectTypeWriterU3Ed__11_System_IDisposable_Dispose_mDC6D61431CC19D94E0267DEC0F98C33B55ED4EE5 (void);
// 0x00000036 System.Boolean TextController/<EffectTypeWriter>d__11::MoveNext()
extern void U3CEffectTypeWriterU3Ed__11_MoveNext_m526247BC8E59E9539D49B82FC5A63E6C68F7528F (void);
// 0x00000037 System.Object TextController/<EffectTypeWriter>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CEffectTypeWriterU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4AB05A21C6E8B855BB783C83976708E17336F40E (void);
// 0x00000038 System.Void TextController/<EffectTypeWriter>d__11::System.Collections.IEnumerator.Reset()
extern void U3CEffectTypeWriterU3Ed__11_System_Collections_IEnumerator_Reset_m5B20599E7F437FC061E2A8016A8CEA064B71366C (void);
// 0x00000039 System.Object TextController/<EffectTypeWriter>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CEffectTypeWriterU3Ed__11_System_Collections_IEnumerator_get_Current_m6C08785C3F6F4452ED3FFC2BE78BCDBC235870B5 (void);
// 0x0000003A System.Void TextController/<hideText>d__12::.ctor(System.Int32)
extern void U3ChideTextU3Ed__12__ctor_mBFF5EF20BA3AF08E2F4CDC5F9FC9EFA602B19880 (void);
// 0x0000003B System.Void TextController/<hideText>d__12::System.IDisposable.Dispose()
extern void U3ChideTextU3Ed__12_System_IDisposable_Dispose_mC840BCA6444410F0DC2044E7E95DF6AF4057194E (void);
// 0x0000003C System.Boolean TextController/<hideText>d__12::MoveNext()
extern void U3ChideTextU3Ed__12_MoveNext_mFA91B82418BE26A3808791FB5D7625A0CD1426B7 (void);
// 0x0000003D System.Object TextController/<hideText>d__12::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3ChideTextU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7A65B53CE309F5C6EB55BD8329EFBED84D9AE54F (void);
// 0x0000003E System.Void TextController/<hideText>d__12::System.Collections.IEnumerator.Reset()
extern void U3ChideTextU3Ed__12_System_Collections_IEnumerator_Reset_mFE32DA56307BDCE2CC1B06B07C0AB139F5F3E48B (void);
// 0x0000003F System.Object TextController/<hideText>d__12::System.Collections.IEnumerator.get_Current()
extern void U3ChideTextU3Ed__12_System_Collections_IEnumerator_get_Current_m076EA8069FB6C232F07CB1221BED99C7E9D06656 (void);
// 0x00000040 System.Void TimedObjectDestructor::Awake()
extern void TimedObjectDestructor_Awake_mB869895487616A6AF866CFCBF989FFD08E6F5FF1 (void);
// 0x00000041 System.Void TimedObjectDestructor::Update()
extern void TimedObjectDestructor_Update_m5ADA3AD96A62F1A6C3C7D272293282C7A1DAFA32 (void);
// 0x00000042 System.Void TimedObjectDestructor::DestroyNow()
extern void TimedObjectDestructor_DestroyNow_mD5E0C96C49D6404F38FA408C72EC8BC481F5E663 (void);
// 0x00000043 System.Void TimedObjectDestructor::.ctor()
extern void TimedObjectDestructor__ctor_m30E4E39EEC802076C629ADFD922C602591F8810D (void);
// 0x00000044 System.Void shootWeapon::Update()
extern void shootWeapon_Update_m77DA5A1650E3FF1510132B27771FDB1A23C1DD40 (void);
// 0x00000045 System.Void shootWeapon::Shoot()
extern void shootWeapon_Shoot_mD72633C4646824CD3530C49362D4476B541FEAF7 (void);
// 0x00000046 System.Void shootWeapon::.ctor()
extern void shootWeapon__ctor_mAC27F34B158060EF9442FF824B50D2135A400246 (void);
static Il2CppMethodPointer s_methodPointers[70] = 
{
	Chaser_Start_mBF0E7582ABA5CEBEE09A80E52BE2869DEA0EF272,
	Chaser_Update_m3242C4DC2FC49423CC5A74FA74D082BCFD4DD1BD,
	Chaser_SetTarget_mDFD1CE91290EA77D3EF0BCC57F41FEC893CA555D,
	Chaser_Attack_mDB506A83B3D6283396537CD22F5A082F02A52EDC,
	Chaser_OnDrawGizmosSelected_m94DAF6A6B20B90C0B4AE7762658F573FC24C1B67,
	Chaser__ctor_m990DFFA4112C0A2012C45A369DEA00CCB2D8C5C3,
	EnemyBehaviour_Start_m3F9AC6A84CB077FDD09407112AE62BF3EA3130E1,
	EnemyBehaviour_Update_m217ADFABABC91E6F24A3AA92B51843F4292E146C,
	EnemyBehaviour_reduceHealth_mAF544A146AD3E2309958558D39D293C7F235F76D,
	EnemyBehaviour__ctor_m64123744E6DEED46299C1DDC37F6D9F625A04556,
	FPCharacterController3D_Start_mBACB9BDE34A7A2C7F5FC03699A8C570C3C84AB10,
	FPCharacterController3D_Update_m9432A3D9BB9526AF736A6042F0A6AA00A1C0E090,
	FPCharacterController3D__ctor_m8F5B5BB7460847852462EC539013455B3C99A3A3,
	GameManager_Start_m26461AEF27E44DB8FECCBC19D6C9E228B658BF8E,
	GameManager_Update_mC9303BA7C3117BD861F49F8E36151CC52117E6C1,
	GameManager_reduceHealth_m8CB257A73E16A1D46EAB9C277187712475A4B5FF,
	GameManager_earnPoints_mC373DB2CBAFC24415EF5236A0432D0CF89B3D049,
	GameManager_gameOver_m0C5438CD9609B29FB90604BB3E3A45196FA4DBAF,
	GameManager_loadSceneAgain_m3FE163CC04E7EAE594080175EB5CAD4CD4B1147E,
	GameManager__ctor_mE8666F6D0CA9C31E16B719F79780DC4B0245B64D,
	U3CloadSceneAgainU3Ed__16__ctor_m98409BEABFB6FEA644054300F989E56D3399AAB8,
	U3CloadSceneAgainU3Ed__16_System_IDisposable_Dispose_mEF81B8CA677E4757F09FD75053F6A84BA67FD5EB,
	U3CloadSceneAgainU3Ed__16_MoveNext_mDBDE5A1A47E38621A3C6B6E6256BA87A7DA62A8D,
	U3CloadSceneAgainU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m66F5E55CD5A6DBBF3D633C476F0BF710F6F5FA79,
	U3CloadSceneAgainU3Ed__16_System_Collections_IEnumerator_Reset_m53B343D1A20609A4314E1A2E772CD046F86FF6F9,
	U3CloadSceneAgainU3Ed__16_System_Collections_IEnumerator_get_Current_m886CFB2E619F5A992E6E0F64A51D4AC0C96238D3,
	MouseLook_Start_m699B23D66C4F21B566C48A524BC40A828F5E3541,
	MouseLook_Update_m3D49361C94E0433BB35499708EE783B4543D83D5,
	MouseLook__ctor_mD12D8075DDEA2085341B59FF8BA9FB353613200B,
	Shooter_Update_mF95D0E122742F65FF3236CC03DDE9CA17DDCB7E2,
	Shooter__ctor_m173C774BF5668018A0B10ACA4BB5A6DA3A6BAF2D,
	Spawner_Start_m61D97BD980BD1B1877634A1E7626E47418D5D6D8,
	Spawner_Spawn_mDE0F1460D4AF60FCE476DA897B2E7EFC2CE7757F,
	Spawner__ctor_m08E8D40AAA40F4329D8A95EEE2B2B6BE842CEB9C,
	U3CSpawnU3Ed__5__ctor_m69706B0276989D786D59CE95A6F860DEE0B2363B,
	U3CSpawnU3Ed__5_System_IDisposable_Dispose_m7F6FCA27C404BA2710C19B5AB653B907AD0DD24F,
	U3CSpawnU3Ed__5_MoveNext_mEFF08C630CFE924711A8775CA21A114B3625B77D,
	U3CSpawnU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m30C2C53935086363A737643AF6C711E1316492AE,
	U3CSpawnU3Ed__5_System_Collections_IEnumerator_Reset_mC20553BB7E3124B38D5CB9842F0728EEDC820D3A,
	U3CSpawnU3Ed__5_System_Collections_IEnumerator_get_Current_mEBD2A653118248414B8194740827EB232443D9A8,
	TextController_Start_m280C87919FD642A897AAB86298E73E3055B1B0F5,
	TextController_Update_m1E0250612056DC745E7ADBF3978B723C7A7854A3,
	TextController_EffectTypeWriter_mB9B8D7C2507F94B53C226E009427E791BEA622C0,
	TextController_hideText_m8A47D7D23A43B14AB88D7228355EA52172D92768,
	TextController__ctor_m62ECE2CB6D0A4F91F707E8FD0D9E288282EAF87D,
	U3CStartU3Ed__9__ctor_m9F7FFB42B5C398095311941B3DB36F77ED7E26D9,
	U3CStartU3Ed__9_System_IDisposable_Dispose_m83DF870E50BD288E98A9623ABB0C09A2FF855695,
	U3CStartU3Ed__9_MoveNext_mA273D2180B707108C2F11738E042D717E1C1B733,
	U3CStartU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE86B6688501ABB91131C54BF7AC5634F60398069,
	U3CStartU3Ed__9_System_Collections_IEnumerator_Reset_mDAE82A4B05CD3A0EF42E1A2AE6624C6929A35B37,
	U3CStartU3Ed__9_System_Collections_IEnumerator_get_Current_m8928E6243C80FA73AD5CF468358680472CF9A755,
	U3CEffectTypeWriterU3Ed__11__ctor_mA232EFA112F6D682C597FB8ED6DA2894CFC3660A,
	U3CEffectTypeWriterU3Ed__11_System_IDisposable_Dispose_mDC6D61431CC19D94E0267DEC0F98C33B55ED4EE5,
	U3CEffectTypeWriterU3Ed__11_MoveNext_m526247BC8E59E9539D49B82FC5A63E6C68F7528F,
	U3CEffectTypeWriterU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4AB05A21C6E8B855BB783C83976708E17336F40E,
	U3CEffectTypeWriterU3Ed__11_System_Collections_IEnumerator_Reset_m5B20599E7F437FC061E2A8016A8CEA064B71366C,
	U3CEffectTypeWriterU3Ed__11_System_Collections_IEnumerator_get_Current_m6C08785C3F6F4452ED3FFC2BE78BCDBC235870B5,
	U3ChideTextU3Ed__12__ctor_mBFF5EF20BA3AF08E2F4CDC5F9FC9EFA602B19880,
	U3ChideTextU3Ed__12_System_IDisposable_Dispose_mC840BCA6444410F0DC2044E7E95DF6AF4057194E,
	U3ChideTextU3Ed__12_MoveNext_mFA91B82418BE26A3808791FB5D7625A0CD1426B7,
	U3ChideTextU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7A65B53CE309F5C6EB55BD8329EFBED84D9AE54F,
	U3ChideTextU3Ed__12_System_Collections_IEnumerator_Reset_mFE32DA56307BDCE2CC1B06B07C0AB139F5F3E48B,
	U3ChideTextU3Ed__12_System_Collections_IEnumerator_get_Current_m076EA8069FB6C232F07CB1221BED99C7E9D06656,
	TimedObjectDestructor_Awake_mB869895487616A6AF866CFCBF989FFD08E6F5FF1,
	TimedObjectDestructor_Update_m5ADA3AD96A62F1A6C3C7D272293282C7A1DAFA32,
	TimedObjectDestructor_DestroyNow_mD5E0C96C49D6404F38FA408C72EC8BC481F5E663,
	TimedObjectDestructor__ctor_m30E4E39EEC802076C629ADFD922C602591F8810D,
	shootWeapon_Update_m77DA5A1650E3FF1510132B27771FDB1A23C1DD40,
	shootWeapon_Shoot_mD72633C4646824CD3530C49362D4476B541FEAF7,
	shootWeapon__ctor_mAC27F34B158060EF9442FF824B50D2135A400246,
};
static const int32_t s_InvokerIndices[70] = 
{
	1972,
	1972,
	1648,
	1972,
	1972,
	1972,
	1972,
	1972,
	1635,
	1972,
	1972,
	1972,
	1972,
	1972,
	1972,
	1972,
	1972,
	1972,
	1920,
	1972,
	1635,
	1972,
	1948,
	1920,
	1972,
	1920,
	1972,
	1972,
	1972,
	1972,
	1972,
	1972,
	1920,
	1972,
	1635,
	1972,
	1948,
	1920,
	1972,
	1920,
	1920,
	1972,
	762,
	1920,
	1972,
	1635,
	1972,
	1948,
	1920,
	1972,
	1920,
	1635,
	1972,
	1948,
	1920,
	1972,
	1920,
	1635,
	1972,
	1948,
	1920,
	1972,
	1920,
	1972,
	1972,
	1972,
	1972,
	1972,
	1972,
	1972,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	70,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
