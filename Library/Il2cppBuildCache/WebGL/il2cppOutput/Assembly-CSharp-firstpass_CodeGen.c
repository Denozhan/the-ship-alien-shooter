﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void UnityStandardAssets.Effects.AfterburnerPhysicsForce::OnEnable()
extern void AfterburnerPhysicsForce_OnEnable_m49E9EB2A249F86D4481E2A55A1769CCC58F883F9 (void);
// 0x00000002 System.Void UnityStandardAssets.Effects.AfterburnerPhysicsForce::FixedUpdate()
extern void AfterburnerPhysicsForce_FixedUpdate_mEFD29CDF2667214C3CEA3FBE7971C2B5565146D9 (void);
// 0x00000003 System.Void UnityStandardAssets.Effects.AfterburnerPhysicsForce::OnDrawGizmosSelected()
extern void AfterburnerPhysicsForce_OnDrawGizmosSelected_mDC329915F6318B476386A63EC2A4E46ABB427F3D (void);
// 0x00000004 System.Void UnityStandardAssets.Effects.AfterburnerPhysicsForce::.ctor()
extern void AfterburnerPhysicsForce__ctor_mC84235E51977F681DB02A1246E1E2BD97E008F5E (void);
// 0x00000005 System.Collections.IEnumerator UnityStandardAssets.Effects.ExplosionFireAndDebris::Start()
extern void ExplosionFireAndDebris_Start_m0990351B5D91DF01F70A4B661657962BF13D526B (void);
// 0x00000006 System.Void UnityStandardAssets.Effects.ExplosionFireAndDebris::AddFire(UnityEngine.Transform,UnityEngine.Vector3,UnityEngine.Vector3)
extern void ExplosionFireAndDebris_AddFire_mEAA60ADA0DC8131CF46299E1777316B9A1AA4C47 (void);
// 0x00000007 System.Void UnityStandardAssets.Effects.ExplosionFireAndDebris::.ctor()
extern void ExplosionFireAndDebris__ctor_m7D18BE077843EB37B07E63B5A5090160D1CFE9C3 (void);
// 0x00000008 System.Void UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>d__4::.ctor(System.Int32)
extern void U3CStartU3Ed__4__ctor_m5DC11C364DE982FC5629EA37B0345C9E546638BF (void);
// 0x00000009 System.Void UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>d__4::System.IDisposable.Dispose()
extern void U3CStartU3Ed__4_System_IDisposable_Dispose_mF216282D7EF841700D69A1DA69A2BBEF658FE5C3 (void);
// 0x0000000A System.Boolean UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>d__4::MoveNext()
extern void U3CStartU3Ed__4_MoveNext_m86E16C3B4F1DA306F00DEEDCAFC67F72CA237E24 (void);
// 0x0000000B System.Object UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9CD55C9B7A75FD557E6E89434FD973925FD67D52 (void);
// 0x0000000C System.Void UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>d__4::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m642B2AA2E5737E4F7B91EEF4B86120332083FC6E (void);
// 0x0000000D System.Object UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m111FBD72FA4B52C98E3BB4E96B4E3B0AED554071 (void);
// 0x0000000E System.Collections.IEnumerator UnityStandardAssets.Effects.ExplosionPhysicsForce::Start()
extern void ExplosionPhysicsForce_Start_m7621BE0822E019E526FEEACB9CB94859983D9328 (void);
// 0x0000000F System.Void UnityStandardAssets.Effects.ExplosionPhysicsForce::.ctor()
extern void ExplosionPhysicsForce__ctor_mFDA3812A754E5BF8D194401B847080B519BA9113 (void);
// 0x00000010 System.Void UnityStandardAssets.Effects.ExplosionPhysicsForce/<Start>d__1::.ctor(System.Int32)
extern void U3CStartU3Ed__1__ctor_m019F7DBE386E3C0F9BFD3B0F132FCBE6DFCA9CF2 (void);
// 0x00000011 System.Void UnityStandardAssets.Effects.ExplosionPhysicsForce/<Start>d__1::System.IDisposable.Dispose()
extern void U3CStartU3Ed__1_System_IDisposable_Dispose_mF43F56524CB4A8589E869718EF267F6B226FF86B (void);
// 0x00000012 System.Boolean UnityStandardAssets.Effects.ExplosionPhysicsForce/<Start>d__1::MoveNext()
extern void U3CStartU3Ed__1_MoveNext_m0311224C526037332CA37473930D1A0072CBE347 (void);
// 0x00000013 System.Object UnityStandardAssets.Effects.ExplosionPhysicsForce/<Start>d__1::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2D2A9F3EBC0A304B5C65BFC9900CE40CFE54171C (void);
// 0x00000014 System.Void UnityStandardAssets.Effects.ExplosionPhysicsForce/<Start>d__1::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__1_System_Collections_IEnumerator_Reset_mCF52863DE7E5B807ADA931A22ECF566512CACBB5 (void);
// 0x00000015 System.Object UnityStandardAssets.Effects.ExplosionPhysicsForce/<Start>d__1::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__1_System_Collections_IEnumerator_get_Current_m49EB09DFFB4023F3806F2E2684F1E318C504D012 (void);
// 0x00000016 System.Void UnityStandardAssets.Effects.Explosive::Start()
extern void Explosive_Start_m890A4F9FCE684A410B1C7E922D1B52BC0808428A (void);
// 0x00000017 System.Collections.IEnumerator UnityStandardAssets.Effects.Explosive::OnCollisionEnter(UnityEngine.Collision)
extern void Explosive_OnCollisionEnter_mA22573333B4F96B8AA12E624BC30BE992CE03550 (void);
// 0x00000018 System.Void UnityStandardAssets.Effects.Explosive::Reset()
extern void Explosive_Reset_mB153DBDB479EDF8DAC09E67CEFA879F2AF941AF6 (void);
// 0x00000019 System.Void UnityStandardAssets.Effects.Explosive::.ctor()
extern void Explosive__ctor_m404F1E4667F58F627F80352149722F5BB9BCF620 (void);
// 0x0000001A System.Void UnityStandardAssets.Effects.Explosive/<OnCollisionEnter>d__7::.ctor(System.Int32)
extern void U3COnCollisionEnterU3Ed__7__ctor_m6F908997A2905D59DFF3A9671CE173D33800F10A (void);
// 0x0000001B System.Void UnityStandardAssets.Effects.Explosive/<OnCollisionEnter>d__7::System.IDisposable.Dispose()
extern void U3COnCollisionEnterU3Ed__7_System_IDisposable_Dispose_mD46B74A99BCDBB6C5B64749578D718C5A3942191 (void);
// 0x0000001C System.Boolean UnityStandardAssets.Effects.Explosive/<OnCollisionEnter>d__7::MoveNext()
extern void U3COnCollisionEnterU3Ed__7_MoveNext_m20F3432132BDFCE18D9158A5D758BE99CAC46FEF (void);
// 0x0000001D System.Object UnityStandardAssets.Effects.Explosive/<OnCollisionEnter>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3COnCollisionEnterU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEC3923221D886833DEB64E1BDC695784F6E28215 (void);
// 0x0000001E System.Void UnityStandardAssets.Effects.Explosive/<OnCollisionEnter>d__7::System.Collections.IEnumerator.Reset()
extern void U3COnCollisionEnterU3Ed__7_System_Collections_IEnumerator_Reset_m2F51DE37499109D49CC1EB4084A48B9BA92E9088 (void);
// 0x0000001F System.Object UnityStandardAssets.Effects.Explosive/<OnCollisionEnter>d__7::System.Collections.IEnumerator.get_Current()
extern void U3COnCollisionEnterU3Ed__7_System_Collections_IEnumerator_get_Current_mFB596E7FCB32B7FF306941A422700C92EE6EC81E (void);
// 0x00000020 System.Void UnityStandardAssets.Effects.ExtinguishableParticleSystem::Start()
extern void ExtinguishableParticleSystem_Start_m98EDC138E24EBD8677DF2EDDFD388419B37F1520 (void);
// 0x00000021 System.Void UnityStandardAssets.Effects.ExtinguishableParticleSystem::Extinguish()
extern void ExtinguishableParticleSystem_Extinguish_mBA6EBAB60B6AC81BA546C68DE5C592412C4667CA (void);
// 0x00000022 System.Void UnityStandardAssets.Effects.ExtinguishableParticleSystem::.ctor()
extern void ExtinguishableParticleSystem__ctor_mBE949E40EF000FEC7D927293FC2168588F49DB1D (void);
// 0x00000023 System.Void UnityStandardAssets.Effects.FireLight::Start()
extern void FireLight_Start_mF7376D4457C906A9DF2BC80035151D54B4E84DC2 (void);
// 0x00000024 System.Void UnityStandardAssets.Effects.FireLight::Update()
extern void FireLight_Update_m0C1C3AB2F812FA06287611ED70E06C6BE0CB3368 (void);
// 0x00000025 System.Void UnityStandardAssets.Effects.FireLight::Extinguish()
extern void FireLight_Extinguish_mBDA0B184B5FB419C9F16403694A1C7690F0C3C3C (void);
// 0x00000026 System.Void UnityStandardAssets.Effects.FireLight::.ctor()
extern void FireLight__ctor_mE607BFC73F207087A6DF3E2B2BC1039D8EC69A20 (void);
// 0x00000027 System.Void UnityStandardAssets.Effects.Hose::Update()
extern void Hose_Update_mE49F942DB22BD437485402B4CC5F4852CD06B9AF (void);
// 0x00000028 System.Void UnityStandardAssets.Effects.Hose::.ctor()
extern void Hose__ctor_mBDB8678C96E8B53F8AA6DF49B57EA037FEC985E7 (void);
// 0x00000029 System.Void UnityStandardAssets.Effects.ParticleSystemMultiplier::Start()
extern void ParticleSystemMultiplier_Start_m11C1C32B5B4257AB76AC11022F340B70C772E020 (void);
// 0x0000002A System.Void UnityStandardAssets.Effects.ParticleSystemMultiplier::.ctor()
extern void ParticleSystemMultiplier__ctor_m0917BF7CE781AEC72EB642D1CB146DAE3EED6901 (void);
// 0x0000002B System.Void UnityStandardAssets.Effects.SmokeParticles::Start()
extern void SmokeParticles_Start_m123815DF8C360DBC83E1D9F354799F8E613C16F1 (void);
// 0x0000002C System.Void UnityStandardAssets.Effects.SmokeParticles::.ctor()
extern void SmokeParticles__ctor_mA213A6962468B2101EEA52C1BA0BA73F1833C53B (void);
// 0x0000002D System.Void UnityStandardAssets.Effects.WaterHoseParticles::Start()
extern void WaterHoseParticles_Start_m607FA501760BADB5421E6882228427C8635FCA99 (void);
// 0x0000002E System.Void UnityStandardAssets.Effects.WaterHoseParticles::OnParticleCollision(UnityEngine.GameObject)
extern void WaterHoseParticles_OnParticleCollision_m077F59C5B3C903778958C7255C3ADFAC4EBB9CFF (void);
// 0x0000002F System.Void UnityStandardAssets.Effects.WaterHoseParticles::.ctor()
extern void WaterHoseParticles__ctor_m37E872633764DCC28D12346104AF9E5D9EF63410 (void);
static Il2CppMethodPointer s_methodPointers[47] = 
{
	AfterburnerPhysicsForce_OnEnable_m49E9EB2A249F86D4481E2A55A1769CCC58F883F9,
	AfterburnerPhysicsForce_FixedUpdate_mEFD29CDF2667214C3CEA3FBE7971C2B5565146D9,
	AfterburnerPhysicsForce_OnDrawGizmosSelected_mDC329915F6318B476386A63EC2A4E46ABB427F3D,
	AfterburnerPhysicsForce__ctor_mC84235E51977F681DB02A1246E1E2BD97E008F5E,
	ExplosionFireAndDebris_Start_m0990351B5D91DF01F70A4B661657962BF13D526B,
	ExplosionFireAndDebris_AddFire_mEAA60ADA0DC8131CF46299E1777316B9A1AA4C47,
	ExplosionFireAndDebris__ctor_m7D18BE077843EB37B07E63B5A5090160D1CFE9C3,
	U3CStartU3Ed__4__ctor_m5DC11C364DE982FC5629EA37B0345C9E546638BF,
	U3CStartU3Ed__4_System_IDisposable_Dispose_mF216282D7EF841700D69A1DA69A2BBEF658FE5C3,
	U3CStartU3Ed__4_MoveNext_m86E16C3B4F1DA306F00DEEDCAFC67F72CA237E24,
	U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9CD55C9B7A75FD557E6E89434FD973925FD67D52,
	U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m642B2AA2E5737E4F7B91EEF4B86120332083FC6E,
	U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m111FBD72FA4B52C98E3BB4E96B4E3B0AED554071,
	ExplosionPhysicsForce_Start_m7621BE0822E019E526FEEACB9CB94859983D9328,
	ExplosionPhysicsForce__ctor_mFDA3812A754E5BF8D194401B847080B519BA9113,
	U3CStartU3Ed__1__ctor_m019F7DBE386E3C0F9BFD3B0F132FCBE6DFCA9CF2,
	U3CStartU3Ed__1_System_IDisposable_Dispose_mF43F56524CB4A8589E869718EF267F6B226FF86B,
	U3CStartU3Ed__1_MoveNext_m0311224C526037332CA37473930D1A0072CBE347,
	U3CStartU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2D2A9F3EBC0A304B5C65BFC9900CE40CFE54171C,
	U3CStartU3Ed__1_System_Collections_IEnumerator_Reset_mCF52863DE7E5B807ADA931A22ECF566512CACBB5,
	U3CStartU3Ed__1_System_Collections_IEnumerator_get_Current_m49EB09DFFB4023F3806F2E2684F1E318C504D012,
	Explosive_Start_m890A4F9FCE684A410B1C7E922D1B52BC0808428A,
	Explosive_OnCollisionEnter_mA22573333B4F96B8AA12E624BC30BE992CE03550,
	Explosive_Reset_mB153DBDB479EDF8DAC09E67CEFA879F2AF941AF6,
	Explosive__ctor_m404F1E4667F58F627F80352149722F5BB9BCF620,
	U3COnCollisionEnterU3Ed__7__ctor_m6F908997A2905D59DFF3A9671CE173D33800F10A,
	U3COnCollisionEnterU3Ed__7_System_IDisposable_Dispose_mD46B74A99BCDBB6C5B64749578D718C5A3942191,
	U3COnCollisionEnterU3Ed__7_MoveNext_m20F3432132BDFCE18D9158A5D758BE99CAC46FEF,
	U3COnCollisionEnterU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEC3923221D886833DEB64E1BDC695784F6E28215,
	U3COnCollisionEnterU3Ed__7_System_Collections_IEnumerator_Reset_m2F51DE37499109D49CC1EB4084A48B9BA92E9088,
	U3COnCollisionEnterU3Ed__7_System_Collections_IEnumerator_get_Current_mFB596E7FCB32B7FF306941A422700C92EE6EC81E,
	ExtinguishableParticleSystem_Start_m98EDC138E24EBD8677DF2EDDFD388419B37F1520,
	ExtinguishableParticleSystem_Extinguish_mBA6EBAB60B6AC81BA546C68DE5C592412C4667CA,
	ExtinguishableParticleSystem__ctor_mBE949E40EF000FEC7D927293FC2168588F49DB1D,
	FireLight_Start_mF7376D4457C906A9DF2BC80035151D54B4E84DC2,
	FireLight_Update_m0C1C3AB2F812FA06287611ED70E06C6BE0CB3368,
	FireLight_Extinguish_mBDA0B184B5FB419C9F16403694A1C7690F0C3C3C,
	FireLight__ctor_mE607BFC73F207087A6DF3E2B2BC1039D8EC69A20,
	Hose_Update_mE49F942DB22BD437485402B4CC5F4852CD06B9AF,
	Hose__ctor_mBDB8678C96E8B53F8AA6DF49B57EA037FEC985E7,
	ParticleSystemMultiplier_Start_m11C1C32B5B4257AB76AC11022F340B70C772E020,
	ParticleSystemMultiplier__ctor_m0917BF7CE781AEC72EB642D1CB146DAE3EED6901,
	SmokeParticles_Start_m123815DF8C360DBC83E1D9F354799F8E613C16F1,
	SmokeParticles__ctor_mA213A6962468B2101EEA52C1BA0BA73F1833C53B,
	WaterHoseParticles_Start_m607FA501760BADB5421E6882228427C8635FCA99,
	WaterHoseParticles_OnParticleCollision_m077F59C5B3C903778958C7255C3ADFAC4EBB9CFF,
	WaterHoseParticles__ctor_m37E872633764DCC28D12346104AF9E5D9EF63410,
};
static const int32_t s_InvokerIndices[47] = 
{
	1972,
	1972,
	1972,
	1972,
	1920,
	653,
	1972,
	1635,
	1972,
	1948,
	1920,
	1972,
	1920,
	1920,
	1972,
	1635,
	1972,
	1948,
	1920,
	1972,
	1920,
	1972,
	1279,
	1972,
	1972,
	1635,
	1972,
	1948,
	1920,
	1972,
	1920,
	1972,
	1972,
	1972,
	1972,
	1972,
	1972,
	1972,
	1972,
	1972,
	1972,
	1972,
	1972,
	1972,
	1972,
	1648,
	1972,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharpU2Dfirstpass_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharpU2Dfirstpass_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpU2Dfirstpass_CodeGenModule = 
{
	"Assembly-CSharp-firstpass.dll",
	47,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharpU2Dfirstpass_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
