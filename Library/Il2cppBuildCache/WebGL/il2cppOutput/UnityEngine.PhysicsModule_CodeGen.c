﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 UnityEngine.Vector3 UnityEngine.Collision::get_relativeVelocity()
extern void Collision_get_relativeVelocity_m0B0F8FA1AFAF7AB3B76083932D63A3FC1FA22442 (void);
// 0x00000002 UnityEngine.ContactPoint[] UnityEngine.Collision::get_contacts()
extern void Collision_get_contacts_m8C3D39F3332DD2AC623A9FB5F2127CE2754AF54B (void);
// 0x00000003 UnityEngine.Collider UnityEngine.RaycastHit::get_collider()
extern void RaycastHit_get_collider_m13A3DE16FBC631E0A1F987E0B22CE70AF8AB539E (void);
// 0x00000004 UnityEngine.Vector3 UnityEngine.RaycastHit::get_point()
extern void RaycastHit_get_point_m32F7282CBB2E13393A33BAD046BDA218E48DD21E (void);
// 0x00000005 UnityEngine.Vector3 UnityEngine.RaycastHit::get_normal()
extern void RaycastHit_get_normal_m2C813B25BAECD87FD9E9CB294278B291F4CC6674 (void);
// 0x00000006 System.Single UnityEngine.RaycastHit::get_distance()
extern void RaycastHit_get_distance_m85FCA98D7957C3BF1D449CA1B48C116CCD6226FA (void);
// 0x00000007 UnityEngine.Transform UnityEngine.RaycastHit::get_transform()
extern void RaycastHit_get_transform_m2DD983DBD3602DE848DE287EE5233FD02EEC608D (void);
// 0x00000008 UnityEngine.Rigidbody UnityEngine.RaycastHit::get_rigidbody()
extern void RaycastHit_get_rigidbody_mE48E45893FDAFD03162C6A73AAF02C6CB0E079FB (void);
// 0x00000009 System.Void UnityEngine.Rigidbody::MovePosition(UnityEngine.Vector3)
extern void Rigidbody_MovePosition_mB3CBBF21FD0ABB88BC6C004B993DED25673001C7 (void);
// 0x0000000A System.Void UnityEngine.Rigidbody::AddForce(UnityEngine.Vector3,UnityEngine.ForceMode)
extern void Rigidbody_AddForce_m78B9D94F505E19F3C63461362AD6DE7EA0836700 (void);
// 0x0000000B System.Void UnityEngine.Rigidbody::AddForceAtPosition(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.ForceMode)
extern void Rigidbody_AddForceAtPosition_mEE49C058A6D57C1D5A78207494BFED5906D80D0F (void);
// 0x0000000C System.Void UnityEngine.Rigidbody::AddForceAtPosition(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Rigidbody_AddForceAtPosition_m5190249D95CE1882B37481C5BFD2ABF201902BA5 (void);
// 0x0000000D System.Void UnityEngine.Rigidbody::AddExplosionForce(System.Single,UnityEngine.Vector3,System.Single,System.Single,UnityEngine.ForceMode)
extern void Rigidbody_AddExplosionForce_mA81BFBF84914CEA89D18047ADE14B47D171280DD (void);
// 0x0000000E System.Void UnityEngine.Rigidbody::.ctor()
extern void Rigidbody__ctor_m0E43BA3B0E70E71B2CA62B165EE5B7CFAEFACDE9 (void);
// 0x0000000F System.Void UnityEngine.Rigidbody::MovePosition_Injected(UnityEngine.Vector3&)
extern void Rigidbody_MovePosition_Injected_m06454253A0DF550B2EAD47F545734E8735BA0732 (void);
// 0x00000010 System.Void UnityEngine.Rigidbody::AddForce_Injected(UnityEngine.Vector3&,UnityEngine.ForceMode)
extern void Rigidbody_AddForce_Injected_m233C3E22C3FE9D2BCBBC510132B82CE26057370C (void);
// 0x00000011 System.Void UnityEngine.Rigidbody::AddForceAtPosition_Injected(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.ForceMode)
extern void Rigidbody_AddForceAtPosition_Injected_m60ED364228EF475F97B0FF1D0F4EFCAB97382DDB (void);
// 0x00000012 System.Void UnityEngine.Rigidbody::AddExplosionForce_Injected(System.Single,UnityEngine.Vector3&,System.Single,System.Single,UnityEngine.ForceMode)
extern void Rigidbody_AddExplosionForce_Injected_m8CCDC7FDD8F5F1BC231094105B3076815A16E22D (void);
// 0x00000013 System.Boolean UnityEngine.Collider::get_enabled()
extern void Collider_get_enabled_m03B73B5C97033F939387D1785BDF2619CADAEEB0 (void);
// 0x00000014 System.Void UnityEngine.Collider::set_enabled(System.Boolean)
extern void Collider_set_enabled_m047B4D830755CD36671F7A60BFAA9C0D61F6C4A1 (void);
// 0x00000015 UnityEngine.Rigidbody UnityEngine.Collider::get_attachedRigidbody()
extern void Collider_get_attachedRigidbody_m101FED12AD292F372F98E94A6D02A5E428AA896A (void);
// 0x00000016 UnityEngine.Vector3 UnityEngine.Collider::ClosestPoint(UnityEngine.Vector3)
extern void Collider_ClosestPoint_m7777917E298B31796DEE906B54F0102F6ED76676 (void);
// 0x00000017 UnityEngine.RaycastHit UnityEngine.Collider::Raycast(UnityEngine.Ray,System.Single,System.Boolean&)
extern void Collider_Raycast_mEB5EDB2C67ABBA9929AE8A898660641E8C82E609 (void);
// 0x00000018 System.Boolean UnityEngine.Collider::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single)
extern void Collider_Raycast_m41CA5C3C07B92F5325CB81890BE3A611C3C70784 (void);
// 0x00000019 System.Void UnityEngine.Collider::.ctor()
extern void Collider__ctor_m09D7A9B985D74FD50346DA08D88EB1874E968B69 (void);
// 0x0000001A System.Void UnityEngine.Collider::ClosestPoint_Injected(UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void Collider_ClosestPoint_Injected_m6D72FF73D51838EE47234CB4D6234521C08B780D (void);
// 0x0000001B System.Void UnityEngine.Collider::Raycast_Injected(UnityEngine.Ray&,System.Single,System.Boolean&,UnityEngine.RaycastHit&)
extern void Collider_Raycast_Injected_m7176D9E8617D67331E20F519855B8708C63B9E9E (void);
// 0x0000001C UnityEngine.CollisionFlags UnityEngine.CharacterController::Move(UnityEngine.Vector3)
extern void CharacterController_Move_mE0EBC32C72A0BEC18EDEBE748D44309A4BA32E60 (void);
// 0x0000001D UnityEngine.CollisionFlags UnityEngine.CharacterController::Move_Injected(UnityEngine.Vector3&)
extern void CharacterController_Move_Injected_m6A2168A4CDC70CB62E4A4DCB15A74133E8C6C46D (void);
// 0x0000001E UnityEngine.Vector3 UnityEngine.SphereCollider::get_center()
extern void SphereCollider_get_center_mBFAE4FFFC76B8FD8F1B2B2F12C52A30470443D3A (void);
// 0x0000001F System.Void UnityEngine.SphereCollider::set_center(UnityEngine.Vector3)
extern void SphereCollider_set_center_mD5E898A2FED304A82BC67ABB11B60BB0F612CED7 (void);
// 0x00000020 System.Single UnityEngine.SphereCollider::get_radius()
extern void SphereCollider_get_radius_m403989140BDAD513299276953B481167CF08D02F (void);
// 0x00000021 System.Void UnityEngine.SphereCollider::set_radius(System.Single)
extern void SphereCollider_set_radius_m55A0D144B32871AECC2A83FBCF423FBE1E5A63A0 (void);
// 0x00000022 System.Void UnityEngine.SphereCollider::get_center_Injected(UnityEngine.Vector3&)
extern void SphereCollider_get_center_Injected_mF082CC62A7E8DE3DD5B86518C210D85139DD93CB (void);
// 0x00000023 System.Void UnityEngine.SphereCollider::set_center_Injected(UnityEngine.Vector3&)
extern void SphereCollider_set_center_Injected_mB213D6CB194E150BBEF8EC2AA31EA391C2F4A641 (void);
// 0x00000024 UnityEngine.Vector3 UnityEngine.ContactPoint::get_point()
extern void ContactPoint_get_point_mEA976D5E3BC57FAB78F68BE0AA17A97293AEA5BC (void);
// 0x00000025 UnityEngine.Vector3 UnityEngine.ContactPoint::get_normal()
extern void ContactPoint_get_normal_m0561937E45F5356C7BB90D861422BD76B36D037A (void);
// 0x00000026 System.String UnityEngine.PhysicsScene::ToString()
extern void PhysicsScene_ToString_mBB6D0AC1E3E2EDC34CB4A7A34485B24B6271903F (void);
// 0x00000027 System.Int32 UnityEngine.PhysicsScene::GetHashCode()
extern void PhysicsScene_GetHashCode_m5344CC6CCCB1CA6EBB149775EE028089DD74B8A2 (void);
// 0x00000028 System.Boolean UnityEngine.PhysicsScene::Equals(System.Object)
extern void PhysicsScene_Equals_m2645ABB96C598F197F734EB712494795928CBCEC (void);
// 0x00000029 System.Boolean UnityEngine.PhysicsScene::Equals(UnityEngine.PhysicsScene)
extern void PhysicsScene_Equals_m8C948B86D105177D519A3608816CDC698C8686B8 (void);
// 0x0000002A System.Boolean UnityEngine.PhysicsScene::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_Raycast_m8C3E61EB7C3DB8AAC6FEB098D815062BEF821187 (void);
// 0x0000002B System.Boolean UnityEngine.PhysicsScene::Internal_RaycastTest(UnityEngine.PhysicsScene,UnityEngine.Ray,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_Internal_RaycastTest_m6715CAD8D0330195269AA7FCCD91613BC564E3EB (void);
// 0x0000002C System.Boolean UnityEngine.PhysicsScene::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_Raycast_mFC0D59C4439EE9DA6E8AA5F6891915132D587208 (void);
// 0x0000002D System.Boolean UnityEngine.PhysicsScene::Internal_Raycast(UnityEngine.PhysicsScene,UnityEngine.Ray,System.Single,UnityEngine.RaycastHit&,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_Internal_Raycast_m1E630AA11805114B090FC50741AEF64D677AF2C7 (void);
// 0x0000002E System.Int32 UnityEngine.PhysicsScene::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit[],System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_Raycast_m95CAB68EB9165E3AB2A4D441F7DF9767AD4B7F73 (void);
// 0x0000002F System.Int32 UnityEngine.PhysicsScene::Internal_RaycastNonAlloc(UnityEngine.PhysicsScene,UnityEngine.Ray,UnityEngine.RaycastHit[],System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_Internal_RaycastNonAlloc_mE7833EE5F1082431A4C2D29362F5DCDEFBF6D2BD (void);
// 0x00000030 System.Boolean UnityEngine.PhysicsScene::Internal_RaycastTest_Injected(UnityEngine.PhysicsScene&,UnityEngine.Ray&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_Internal_RaycastTest_Injected_m956474EFEA507F82ABE0BCB75DDB3CC8EFF4D12B (void);
// 0x00000031 System.Boolean UnityEngine.PhysicsScene::Internal_Raycast_Injected(UnityEngine.PhysicsScene&,UnityEngine.Ray&,System.Single,UnityEngine.RaycastHit&,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_Internal_Raycast_Injected_m8868E0BF89F8C42D11F42F94F9CF767647433BB1 (void);
// 0x00000032 System.Int32 UnityEngine.PhysicsScene::Internal_RaycastNonAlloc_Injected(UnityEngine.PhysicsScene&,UnityEngine.Ray&,UnityEngine.RaycastHit[],System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_Internal_RaycastNonAlloc_Injected_m59FB44C18E62D21E9320F30229C0AA9F9B971211 (void);
// 0x00000033 UnityEngine.PhysicsScene UnityEngine.Physics::get_defaultPhysicsScene()
extern void Physics_get_defaultPhysicsScene_mA6361488FBAC110DA8397E5E4CB473EBF4191010 (void);
// 0x00000034 System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_Raycast_m9DE0EEA1CF8DEF7D06216225F19E2958D341F7BA (void);
// 0x00000035 System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32)
extern void Physics_Raycast_m09F21E13465121A73F19C07FC5F5314CF15ACD15 (void);
// 0x00000036 System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void Physics_Raycast_m284670765E1627E43B7B0F5EC811A688EE595091 (void);
// 0x00000037 System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Physics_Raycast_mDA2EB8C7692308A7178222D31CBA4C7A1C7DC915 (void);
// 0x00000038 System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_Raycast_m2EA572B4613E1BD7DBAA299511CFD2DBA179A163 (void);
// 0x00000039 System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32)
extern void Physics_Raycast_mCBD5F7D498C246713EDDBB446E97205DA206C49C (void);
// 0x0000003A System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single)
extern void Physics_Raycast_m18E12C65F127D1AA50D196623F04F81CB138FD12 (void);
// 0x0000003B System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&)
extern void Physics_Raycast_mE84D30EEFE59DA28DA172342068F092A35B2BE4A (void);
// 0x0000003C System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_Raycast_m77560CCAA49DC821E51FDDD1570B921D7704646F (void);
// 0x0000003D System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,System.Single,System.Int32)
extern void Physics_Raycast_mFDC4B8E7979495E3C22D0E3CEA4BCAB271EEC25A (void);
// 0x0000003E System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,System.Single)
extern void Physics_Raycast_mD7A711D3A790AC505F7229A4FFFA2E389008176B (void);
// 0x0000003F System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray)
extern void Physics_Raycast_m111AFC36A136A67BE4776670E356E99A82010BFE (void);
// 0x00000040 System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_Raycast_mCA3F2DD1DC08199AAD8466BB857318CD454AC774 (void);
// 0x00000041 System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single,System.Int32)
extern void Physics_Raycast_m46E12070D6996F4C8C91D49ADC27C74AC1D6A3D8 (void);
// 0x00000042 System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single)
extern void Physics_Raycast_mA64F8C30681E3A6A8F2B7EDE592FE7BBC0D354F4 (void);
// 0x00000043 System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&)
extern void Physics_Raycast_m80EC8EEDA0ABA8B01838BA9054834CD1A381916E (void);
// 0x00000044 UnityEngine.RaycastHit[] UnityEngine.Physics::Internal_RaycastAll(UnityEngine.PhysicsScene,UnityEngine.Ray,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_Internal_RaycastAll_m39EE7CF896F9D8BA58CC623F14E4DB0641480523 (void);
// 0x00000045 UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_RaycastAll_m5103E7C60CC66BAFA6534D1736138A92EB1EF2B8 (void);
// 0x00000046 UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32)
extern void Physics_RaycastAll_m2C977C8B022672F42B5E18F40B529C0A74B7E471 (void);
// 0x00000047 UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void Physics_RaycastAll_m2BBC8D2731B38EE0B704A5C6A09D0F8BBA074A4D (void);
// 0x00000048 UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Physics_RaycastAll_m83F28CB671653C07995FB1BCDC561121DE3D9CA6 (void);
// 0x00000049 UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Ray,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_RaycastAll_m55EB4478198ED6EF838500257FA3BE1A871D3605 (void);
// 0x0000004A UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Ray,System.Single,System.Int32)
extern void Physics_RaycastAll_mA24B9B922C98C5D18397FAE55C04AEAFDD47F029 (void);
// 0x0000004B UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Ray,System.Single)
extern void Physics_RaycastAll_m72947571EFB0EFB34E48340AA2EC0C8030D27C50 (void);
// 0x0000004C UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Ray)
extern void Physics_RaycastAll_m529EE59D6D03E4CFAECE4016C8CC4181BEC2D26D (void);
// 0x0000004D System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Ray,UnityEngine.RaycastHit[],System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_RaycastNonAlloc_m7C81125AD7D5891EBC3AB48C6DED7B60F53DF099 (void);
// 0x0000004E System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Ray,UnityEngine.RaycastHit[],System.Single,System.Int32)
extern void Physics_RaycastNonAlloc_m8F62EE5A33E81A02E4983A171023B7BC4AC700CA (void);
// 0x0000004F System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Ray,UnityEngine.RaycastHit[],System.Single)
extern void Physics_RaycastNonAlloc_m41BB7BB8755B09700C10F59A29C3541D45AB9CCD (void);
// 0x00000050 System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Ray,UnityEngine.RaycastHit[])
extern void Physics_RaycastNonAlloc_mBC5BE514E55B8D98A8F4752DED6850E02EE8AD98 (void);
// 0x00000051 System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit[],System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_RaycastNonAlloc_m2C90D14E472DE7929EFD54FDA7BC512D3FBDE4ED (void);
// 0x00000052 System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit[],System.Single,System.Int32)
extern void Physics_RaycastNonAlloc_m316F597067055C9F923F57CC68D68FF917C3B4D1 (void);
// 0x00000053 System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit[],System.Single)
extern void Physics_RaycastNonAlloc_m1B8F31BF41F756F561F6AC3A2E0736F2BC9C4DB4 (void);
// 0x00000054 System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit[])
extern void Physics_RaycastNonAlloc_m9076DDE1A65F87DB3D2824DAD4E5B8B534159F1F (void);
// 0x00000055 UnityEngine.Collider[] UnityEngine.Physics::OverlapSphere_Internal(UnityEngine.PhysicsScene,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_OverlapSphere_Internal_m92572431C76930C21D10CEFC0F0D37B945A6F4FF (void);
// 0x00000056 UnityEngine.Collider[] UnityEngine.Physics::OverlapSphere(UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_OverlapSphere_m1CDA8916BA89EE2B497158A08CD571044D60054B (void);
// 0x00000057 UnityEngine.Collider[] UnityEngine.Physics::OverlapSphere(UnityEngine.Vector3,System.Single,System.Int32)
extern void Physics_OverlapSphere_mE1FC40C646B1468905057516601DB49DD41E0223 (void);
// 0x00000058 UnityEngine.Collider[] UnityEngine.Physics::OverlapSphere(UnityEngine.Vector3,System.Single)
extern void Physics_OverlapSphere_mE4A0577DF7C0681DE7FFC4F2A2C1BFB8D402CA0C (void);
// 0x00000059 System.Boolean UnityEngine.Physics::CheckSphere_Internal(UnityEngine.PhysicsScene,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_CheckSphere_Internal_m2139E9BBEC789DCDCA7A729F13A0AB6FD7E4ADAA (void);
// 0x0000005A System.Boolean UnityEngine.Physics::CheckSphere(UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_CheckSphere_mF4AE4778A415A4E9C7C15BA21A0E402909AD3472 (void);
// 0x0000005B System.Boolean UnityEngine.Physics::CheckSphere(UnityEngine.Vector3,System.Single,System.Int32)
extern void Physics_CheckSphere_m6DFD61C841CEBFDE6645689279AA6E31297B002B (void);
// 0x0000005C System.Void UnityEngine.Physics::get_defaultPhysicsScene_Injected(UnityEngine.PhysicsScene&)
extern void Physics_get_defaultPhysicsScene_Injected_m0B71068269B3C8499A258BBCAEA7E3D9D020BADA (void);
// 0x0000005D UnityEngine.RaycastHit[] UnityEngine.Physics::Internal_RaycastAll_Injected(UnityEngine.PhysicsScene&,UnityEngine.Ray&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_Internal_RaycastAll_Injected_mD4C3E8762D9FCEE654CE0415E636EB7DBEC92A5B (void);
// 0x0000005E UnityEngine.Collider[] UnityEngine.Physics::OverlapSphere_Internal_Injected(UnityEngine.PhysicsScene&,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_OverlapSphere_Internal_Injected_mBC7164D0CB4C0ED3299EE6515F162F324C16D291 (void);
// 0x0000005F System.Boolean UnityEngine.Physics::CheckSphere_Internal_Injected(UnityEngine.PhysicsScene&,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_CheckSphere_Internal_Injected_mEF8DD496E66AB1FDBF6EE8E10831E795B7B77A00 (void);
static Il2CppMethodPointer s_methodPointers[95] = 
{
	Collision_get_relativeVelocity_m0B0F8FA1AFAF7AB3B76083932D63A3FC1FA22442,
	Collision_get_contacts_m8C3D39F3332DD2AC623A9FB5F2127CE2754AF54B,
	RaycastHit_get_collider_m13A3DE16FBC631E0A1F987E0B22CE70AF8AB539E,
	RaycastHit_get_point_m32F7282CBB2E13393A33BAD046BDA218E48DD21E,
	RaycastHit_get_normal_m2C813B25BAECD87FD9E9CB294278B291F4CC6674,
	RaycastHit_get_distance_m85FCA98D7957C3BF1D449CA1B48C116CCD6226FA,
	RaycastHit_get_transform_m2DD983DBD3602DE848DE287EE5233FD02EEC608D,
	RaycastHit_get_rigidbody_mE48E45893FDAFD03162C6A73AAF02C6CB0E079FB,
	Rigidbody_MovePosition_mB3CBBF21FD0ABB88BC6C004B993DED25673001C7,
	Rigidbody_AddForce_m78B9D94F505E19F3C63461362AD6DE7EA0836700,
	Rigidbody_AddForceAtPosition_mEE49C058A6D57C1D5A78207494BFED5906D80D0F,
	Rigidbody_AddForceAtPosition_m5190249D95CE1882B37481C5BFD2ABF201902BA5,
	Rigidbody_AddExplosionForce_mA81BFBF84914CEA89D18047ADE14B47D171280DD,
	Rigidbody__ctor_m0E43BA3B0E70E71B2CA62B165EE5B7CFAEFACDE9,
	Rigidbody_MovePosition_Injected_m06454253A0DF550B2EAD47F545734E8735BA0732,
	Rigidbody_AddForce_Injected_m233C3E22C3FE9D2BCBBC510132B82CE26057370C,
	Rigidbody_AddForceAtPosition_Injected_m60ED364228EF475F97B0FF1D0F4EFCAB97382DDB,
	Rigidbody_AddExplosionForce_Injected_m8CCDC7FDD8F5F1BC231094105B3076815A16E22D,
	Collider_get_enabled_m03B73B5C97033F939387D1785BDF2619CADAEEB0,
	Collider_set_enabled_m047B4D830755CD36671F7A60BFAA9C0D61F6C4A1,
	Collider_get_attachedRigidbody_m101FED12AD292F372F98E94A6D02A5E428AA896A,
	Collider_ClosestPoint_m7777917E298B31796DEE906B54F0102F6ED76676,
	Collider_Raycast_mEB5EDB2C67ABBA9929AE8A898660641E8C82E609,
	Collider_Raycast_m41CA5C3C07B92F5325CB81890BE3A611C3C70784,
	Collider__ctor_m09D7A9B985D74FD50346DA08D88EB1874E968B69,
	Collider_ClosestPoint_Injected_m6D72FF73D51838EE47234CB4D6234521C08B780D,
	Collider_Raycast_Injected_m7176D9E8617D67331E20F519855B8708C63B9E9E,
	CharacterController_Move_mE0EBC32C72A0BEC18EDEBE748D44309A4BA32E60,
	CharacterController_Move_Injected_m6A2168A4CDC70CB62E4A4DCB15A74133E8C6C46D,
	SphereCollider_get_center_mBFAE4FFFC76B8FD8F1B2B2F12C52A30470443D3A,
	SphereCollider_set_center_mD5E898A2FED304A82BC67ABB11B60BB0F612CED7,
	SphereCollider_get_radius_m403989140BDAD513299276953B481167CF08D02F,
	SphereCollider_set_radius_m55A0D144B32871AECC2A83FBCF423FBE1E5A63A0,
	SphereCollider_get_center_Injected_mF082CC62A7E8DE3DD5B86518C210D85139DD93CB,
	SphereCollider_set_center_Injected_mB213D6CB194E150BBEF8EC2AA31EA391C2F4A641,
	ContactPoint_get_point_mEA976D5E3BC57FAB78F68BE0AA17A97293AEA5BC,
	ContactPoint_get_normal_m0561937E45F5356C7BB90D861422BD76B36D037A,
	PhysicsScene_ToString_mBB6D0AC1E3E2EDC34CB4A7A34485B24B6271903F,
	PhysicsScene_GetHashCode_m5344CC6CCCB1CA6EBB149775EE028089DD74B8A2,
	PhysicsScene_Equals_m2645ABB96C598F197F734EB712494795928CBCEC,
	PhysicsScene_Equals_m8C948B86D105177D519A3608816CDC698C8686B8,
	PhysicsScene_Raycast_m8C3E61EB7C3DB8AAC6FEB098D815062BEF821187,
	PhysicsScene_Internal_RaycastTest_m6715CAD8D0330195269AA7FCCD91613BC564E3EB,
	PhysicsScene_Raycast_mFC0D59C4439EE9DA6E8AA5F6891915132D587208,
	PhysicsScene_Internal_Raycast_m1E630AA11805114B090FC50741AEF64D677AF2C7,
	PhysicsScene_Raycast_m95CAB68EB9165E3AB2A4D441F7DF9767AD4B7F73,
	PhysicsScene_Internal_RaycastNonAlloc_mE7833EE5F1082431A4C2D29362F5DCDEFBF6D2BD,
	PhysicsScene_Internal_RaycastTest_Injected_m956474EFEA507F82ABE0BCB75DDB3CC8EFF4D12B,
	PhysicsScene_Internal_Raycast_Injected_m8868E0BF89F8C42D11F42F94F9CF767647433BB1,
	PhysicsScene_Internal_RaycastNonAlloc_Injected_m59FB44C18E62D21E9320F30229C0AA9F9B971211,
	Physics_get_defaultPhysicsScene_mA6361488FBAC110DA8397E5E4CB473EBF4191010,
	Physics_Raycast_m9DE0EEA1CF8DEF7D06216225F19E2958D341F7BA,
	Physics_Raycast_m09F21E13465121A73F19C07FC5F5314CF15ACD15,
	Physics_Raycast_m284670765E1627E43B7B0F5EC811A688EE595091,
	Physics_Raycast_mDA2EB8C7692308A7178222D31CBA4C7A1C7DC915,
	Physics_Raycast_m2EA572B4613E1BD7DBAA299511CFD2DBA179A163,
	Physics_Raycast_mCBD5F7D498C246713EDDBB446E97205DA206C49C,
	Physics_Raycast_m18E12C65F127D1AA50D196623F04F81CB138FD12,
	Physics_Raycast_mE84D30EEFE59DA28DA172342068F092A35B2BE4A,
	Physics_Raycast_m77560CCAA49DC821E51FDDD1570B921D7704646F,
	Physics_Raycast_mFDC4B8E7979495E3C22D0E3CEA4BCAB271EEC25A,
	Physics_Raycast_mD7A711D3A790AC505F7229A4FFFA2E389008176B,
	Physics_Raycast_m111AFC36A136A67BE4776670E356E99A82010BFE,
	Physics_Raycast_mCA3F2DD1DC08199AAD8466BB857318CD454AC774,
	Physics_Raycast_m46E12070D6996F4C8C91D49ADC27C74AC1D6A3D8,
	Physics_Raycast_mA64F8C30681E3A6A8F2B7EDE592FE7BBC0D354F4,
	Physics_Raycast_m80EC8EEDA0ABA8B01838BA9054834CD1A381916E,
	Physics_Internal_RaycastAll_m39EE7CF896F9D8BA58CC623F14E4DB0641480523,
	Physics_RaycastAll_m5103E7C60CC66BAFA6534D1736138A92EB1EF2B8,
	Physics_RaycastAll_m2C977C8B022672F42B5E18F40B529C0A74B7E471,
	Physics_RaycastAll_m2BBC8D2731B38EE0B704A5C6A09D0F8BBA074A4D,
	Physics_RaycastAll_m83F28CB671653C07995FB1BCDC561121DE3D9CA6,
	Physics_RaycastAll_m55EB4478198ED6EF838500257FA3BE1A871D3605,
	Physics_RaycastAll_mA24B9B922C98C5D18397FAE55C04AEAFDD47F029,
	Physics_RaycastAll_m72947571EFB0EFB34E48340AA2EC0C8030D27C50,
	Physics_RaycastAll_m529EE59D6D03E4CFAECE4016C8CC4181BEC2D26D,
	Physics_RaycastNonAlloc_m7C81125AD7D5891EBC3AB48C6DED7B60F53DF099,
	Physics_RaycastNonAlloc_m8F62EE5A33E81A02E4983A171023B7BC4AC700CA,
	Physics_RaycastNonAlloc_m41BB7BB8755B09700C10F59A29C3541D45AB9CCD,
	Physics_RaycastNonAlloc_mBC5BE514E55B8D98A8F4752DED6850E02EE8AD98,
	Physics_RaycastNonAlloc_m2C90D14E472DE7929EFD54FDA7BC512D3FBDE4ED,
	Physics_RaycastNonAlloc_m316F597067055C9F923F57CC68D68FF917C3B4D1,
	Physics_RaycastNonAlloc_m1B8F31BF41F756F561F6AC3A2E0736F2BC9C4DB4,
	Physics_RaycastNonAlloc_m9076DDE1A65F87DB3D2824DAD4E5B8B534159F1F,
	Physics_OverlapSphere_Internal_m92572431C76930C21D10CEFC0F0D37B945A6F4FF,
	Physics_OverlapSphere_m1CDA8916BA89EE2B497158A08CD571044D60054B,
	Physics_OverlapSphere_mE1FC40C646B1468905057516601DB49DD41E0223,
	Physics_OverlapSphere_mE4A0577DF7C0681DE7FFC4F2A2C1BFB8D402CA0C,
	Physics_CheckSphere_Internal_m2139E9BBEC789DCDCA7A729F13A0AB6FD7E4ADAA,
	Physics_CheckSphere_mF4AE4778A415A4E9C7C15BA21A0E402909AD3472,
	Physics_CheckSphere_m6DFD61C841CEBFDE6645689279AA6E31297B002B,
	Physics_get_defaultPhysicsScene_Injected_m0B71068269B3C8499A258BBCAEA7E3D9D020BADA,
	Physics_Internal_RaycastAll_Injected_mD4C3E8762D9FCEE654CE0415E636EB7DBEC92A5B,
	Physics_OverlapSphere_Internal_Injected_mBC7164D0CB4C0ED3299EE6515F162F324C16D291,
	Physics_CheckSphere_Internal_Injected_mEF8DD496E66AB1FDBF6EE8E10831E795B7B77A00,
};
extern void RaycastHit_get_collider_m13A3DE16FBC631E0A1F987E0B22CE70AF8AB539E_AdjustorThunk (void);
extern void RaycastHit_get_point_m32F7282CBB2E13393A33BAD046BDA218E48DD21E_AdjustorThunk (void);
extern void RaycastHit_get_normal_m2C813B25BAECD87FD9E9CB294278B291F4CC6674_AdjustorThunk (void);
extern void RaycastHit_get_distance_m85FCA98D7957C3BF1D449CA1B48C116CCD6226FA_AdjustorThunk (void);
extern void RaycastHit_get_transform_m2DD983DBD3602DE848DE287EE5233FD02EEC608D_AdjustorThunk (void);
extern void RaycastHit_get_rigidbody_mE48E45893FDAFD03162C6A73AAF02C6CB0E079FB_AdjustorThunk (void);
extern void ContactPoint_get_point_mEA976D5E3BC57FAB78F68BE0AA17A97293AEA5BC_AdjustorThunk (void);
extern void ContactPoint_get_normal_m0561937E45F5356C7BB90D861422BD76B36D037A_AdjustorThunk (void);
extern void PhysicsScene_ToString_mBB6D0AC1E3E2EDC34CB4A7A34485B24B6271903F_AdjustorThunk (void);
extern void PhysicsScene_GetHashCode_m5344CC6CCCB1CA6EBB149775EE028089DD74B8A2_AdjustorThunk (void);
extern void PhysicsScene_Equals_m2645ABB96C598F197F734EB712494795928CBCEC_AdjustorThunk (void);
extern void PhysicsScene_Equals_m8C948B86D105177D519A3608816CDC698C8686B8_AdjustorThunk (void);
extern void PhysicsScene_Raycast_m8C3E61EB7C3DB8AAC6FEB098D815062BEF821187_AdjustorThunk (void);
extern void PhysicsScene_Raycast_mFC0D59C4439EE9DA6E8AA5F6891915132D587208_AdjustorThunk (void);
extern void PhysicsScene_Raycast_m95CAB68EB9165E3AB2A4D441F7DF9767AD4B7F73_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[15] = 
{
	{ 0x06000003, RaycastHit_get_collider_m13A3DE16FBC631E0A1F987E0B22CE70AF8AB539E_AdjustorThunk },
	{ 0x06000004, RaycastHit_get_point_m32F7282CBB2E13393A33BAD046BDA218E48DD21E_AdjustorThunk },
	{ 0x06000005, RaycastHit_get_normal_m2C813B25BAECD87FD9E9CB294278B291F4CC6674_AdjustorThunk },
	{ 0x06000006, RaycastHit_get_distance_m85FCA98D7957C3BF1D449CA1B48C116CCD6226FA_AdjustorThunk },
	{ 0x06000007, RaycastHit_get_transform_m2DD983DBD3602DE848DE287EE5233FD02EEC608D_AdjustorThunk },
	{ 0x06000008, RaycastHit_get_rigidbody_mE48E45893FDAFD03162C6A73AAF02C6CB0E079FB_AdjustorThunk },
	{ 0x06000024, ContactPoint_get_point_mEA976D5E3BC57FAB78F68BE0AA17A97293AEA5BC_AdjustorThunk },
	{ 0x06000025, ContactPoint_get_normal_m0561937E45F5356C7BB90D861422BD76B36D037A_AdjustorThunk },
	{ 0x06000026, PhysicsScene_ToString_mBB6D0AC1E3E2EDC34CB4A7A34485B24B6271903F_AdjustorThunk },
	{ 0x06000027, PhysicsScene_GetHashCode_m5344CC6CCCB1CA6EBB149775EE028089DD74B8A2_AdjustorThunk },
	{ 0x06000028, PhysicsScene_Equals_m2645ABB96C598F197F734EB712494795928CBCEC_AdjustorThunk },
	{ 0x06000029, PhysicsScene_Equals_m8C948B86D105177D519A3608816CDC698C8686B8_AdjustorThunk },
	{ 0x0600002A, PhysicsScene_Raycast_m8C3E61EB7C3DB8AAC6FEB098D815062BEF821187_AdjustorThunk },
	{ 0x0600002C, PhysicsScene_Raycast_mFC0D59C4439EE9DA6E8AA5F6891915132D587208_AdjustorThunk },
	{ 0x0600002E, PhysicsScene_Raycast_m95CAB68EB9165E3AB2A4D441F7DF9767AD4B7F73_AdjustorThunk },
};
static const int32_t s_InvokerIndices[95] = 
{
	1968,
	1920,
	1920,
	1968,
	1968,
	1955,
	1920,
	1920,
	1697,
	1043,
	679,
	1046,
	229,
	1972,
	1603,
	856,
	583,
	226,
	1948,
	1674,
	1920,
	1565,
	548,
	578,
	1972,
	854,
	375,
	1225,
	1163,
	1968,
	1697,
	1955,
	1681,
	1603,
	1603,
	1968,
	1968,
	1920,
	1906,
	1437,
	1440,
	174,
	2222,
	107,
	2111,
	91,
	2086,
	2208,
	2104,
	2079,
	3263,
	2226,
	2427,
	2595,
	2886,
	2113,
	2225,
	2426,
	2594,
	2421,
	2591,
	2874,
	3180,
	2224,
	2420,
	2590,
	2873,
	2202,
	2205,
	2391,
	2564,
	2802,
	2388,
	2559,
	2794,
	3148,
	2182,
	2346,
	2517,
	2758,
	2089,
	2184,
	2348,
	2519,
	2203,
	2390,
	2563,
	2801,
	2223,
	2425,
	2593,
	3219,
	2187,
	2187,
	2208,
};
extern const CustomAttributesCacheGenerator g_UnityEngine_PhysicsModule_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_UnityEngine_PhysicsModule_CodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_PhysicsModule_CodeGenModule = 
{
	"UnityEngine.PhysicsModule.dll",
	95,
	s_methodPointers,
	15,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_UnityEngine_PhysicsModule_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
