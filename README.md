# THE SHIP: ALIEN SHOOTER
#### Welcome to the ship!

[![N|Solid](https://img.itch.zone/aW1hZ2UvMTA1ODc4NC82MDU5NzQ3LnBuZw==/347x500/XgmqGc.png)](https://denzhn.itch.io/the-ship-alien-shooter)


- Use your gun to shoot as many as you can.
- Keep your distance from them or they will attack you.
- Listen sounds to find them. Don't forget to look at your behind!

## Features

- Chasing monsters with attack and death animation.
- Random spawning according to the player's position. (not a complex system)
- Post processing effects
- Endless gameplay (you can always try for a new best score)

## Credits

- "Scifi Gun" (https://skfb.ly/6xpqS) by Slav92 is licensed under Creative Commons Attribution (http://creativecommons.org/licenses/by/4.0/).
- Gameplay Music - Conspiracy Theory by Fesliyan Studios 
- 3D Free Modular Kit by Barking Dog (https://assetstore.unity.com/packages/3d/environments/3d-free-modular-kit-85732)
- Monster model and animations were imported from Mixamo

## License

Project is open-source. You can use it like you want. **But don't forget to mention me at credits!**
