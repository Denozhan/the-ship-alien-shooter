﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextController : MonoBehaviour
{
    public Text text1;
    public Text text2;
    public Text text3;
    public Canvas TextCanvas;
    public string string1;
    public string string2;
    public string string3;
    public float waitTime;
    public GameObject spawner;

    // Start is called before the first frame update
    IEnumerator Start()
    {
        yield return StartCoroutine(EffectTypeWriter(string1, text1));
        yield return StartCoroutine(EffectTypeWriter(string2, text2));;
        yield return StartCoroutine(EffectTypeWriter(string3, text3));
        yield return StartCoroutine(hideText());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private IEnumerator EffectTypeWriter(string text, Text desiredText)
    {
        foreach (char character in text.ToCharArray())
        {
            desiredText.text = desiredText.text + character;
            yield return new WaitForSeconds(waitTime);
        }
    }

    private IEnumerator hideText()
    {
        yield return new WaitForSeconds(1f);
        TextCanvas.gameObject.SetActive(false);
        spawner.SetActive(true);
    }
}
