﻿using UnityEngine;
using System.Collections;

//[RequireComponent(typeof(CharacterController))]

public class Chaser : MonoBehaviour {

	public float speed = 20.0f;
	public float minDist = 2f;
	public Transform target;
	public Transform attackPoint;
	public float attackRange;
	public LayerMask enemyLayer;

	private Rigidbody _chaserRigid;
	private Animator _animator;

	// Use this for initialization
	void Start () 
	{
		// if no target specified, assume the player
		if (target == null) {
			if (GameObject.FindWithTag ("Player")!=null)
			{
				target = GameObject.FindWithTag ("Player").GetComponent<Transform>();
			}
		}
		_chaserRigid = GetComponent<Rigidbody>();
		_animator = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (target == null)
			return;

		// face the target
		transform.LookAt(target);

		//get the distance between the chaser and the target
		float distance = Vector3.Distance(transform.position,target.position);

			//so long as the chaser is farther away than the minimum distance, move towards it at rate speed.
		if (distance >= minDist)
		{
			//transform.position += transform.forward * speed * Time.deltaTime;	
			_chaserRigid.MovePosition((transform.position) + transform.forward * speed * Time.deltaTime);
			_animator.SetBool("isAttacking", false);
		}
		else {
			Attack();
		}
	}

	// Set the target of the chaser
	public void SetTarget(Transform newTarget)
	{
		target = newTarget;
	}

	public void Attack()
    {
		if (!GetComponent<EnemyBehaviour>().isPointsEarned)
		{
			_animator.SetBool("isAttacking", true);
			Collider[] hitEnemies = Physics.OverlapSphere(attackPoint.position, attackRange, enemyLayer);
			foreach (Collider coll in hitEnemies)
			{
				GameManager.gm.reduceHealth();
			}
		}
	}

    private void OnDrawGizmosSelected()
    {
        if (attackPoint == null)
        {
			return;
        }
		Gizmos.DrawWireSphere(attackPoint.position, attackRange);
    }

}
