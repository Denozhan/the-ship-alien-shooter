﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms;

public class Spawner : MonoBehaviour
{
    public GameObject[] spawnObjects;
    public GameObject[] spawnPositions;

    [Range(0.0f, 10.0f)]
    public float spawnSpeed = 6f;
    private float _currentSpeed;

    // Start is called before the first frame update
    void Start()
    {
        _currentSpeed = spawnSpeed;
        StartCoroutine(Spawn());
    }


    private IEnumerator Spawn()
    {
        while (true)
        {
            int selectedPosition = UnityEngine.Random.Range(0, spawnPositions.Length);
            int selectedObject = UnityEngine.Random.Range(0,spawnObjects.Length);
            GameObject.Instantiate(spawnObjects[selectedObject], spawnPositions[selectedPosition].transform.position, Quaternion.identity);
            yield return new WaitForSeconds(spawnSpeed);
            if(spawnSpeed > 3f)
            {
                spawnSpeed = spawnSpeed - 0.1f;
            }
            else
            {
                _currentSpeed = _currentSpeed - 0.1f;
                spawnSpeed = _currentSpeed;
            }
        }
    }
    
}
