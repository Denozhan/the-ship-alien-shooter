using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager gm;
    public float health = 100f;
    public Text healthText;
    public Text bestScoreText;
    public Text scoreText;
    public Text newBestAnimation;
    public Canvas gameOverCanvas;
    public Canvas levelCanvas;
    public Camera mainCamera;
    public bool isGameOver;

    private int _points;
    // Start is called before the first frame update
    void Start()
    {
        _points = 0;
        newBestAnimation.gameObject.SetActive(false);
        bestScoreText.text = "Best Score: " + PlayerPrefs.GetInt("BestScore");
        scoreText.text = "Score: " + _points;
        Time.timeScale = 1f;
        gm = GetComponent<GameManager>();
        isGameOver = false;
        healthText.text = "Current Health: " + health;
        gameOverCanvas.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (isGameOver)
        {
            gameOver();
        }
    }

    public void reduceHealth()
    {
        if (health > 0)
        {
            health = health - 1;
            healthText.text = "Current Health: " + health;
        }
        else if (health <= 0)
        {
            isGameOver = true;
        }
    }

    public void earnPoints()
    {
        _points = _points + 10;
        scoreText.text = "Score: " + _points;
    }

    public void gameOver()
    {
        if(PlayerPrefs.GetInt("BestScore")< _points)
        {
            newBestAnimation.gameObject.SetActive(true);
            PlayerPrefs.SetInt("BestScore",_points);
        }
        levelCanvas.gameObject.SetActive(false);
        gameOverCanvas.gameObject.SetActive(true);
        StartCoroutine("loadSceneAgain");
        Time.timeScale = 0f;
    }

    public IEnumerator loadSceneAgain()
    {
        yield return new WaitForSecondsRealtime(3f);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
