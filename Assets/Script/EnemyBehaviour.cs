using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehaviour : MonoBehaviour
{
    public GameObject enemySound;
    public int health;
    public bool isPointsEarned;

    private Animator _animator;
    private AudioSource _enemySFX;

    // Start is called before the first frame update
    void Start()
    {
        _animator = GetComponent<Animator>();
        _enemySFX = enemySound.GetComponent<AudioSource>();
        _enemySFX.Play();
        isPointsEarned = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (health <= 0)
        {
            _enemySFX.GetComponent<AudioSource>().Stop();
            _animator.SetBool("isAttacking", false);
            _animator.SetBool("isDead",true);
            GetComponent<Collider>().enabled = false;
            GetComponent<Chaser>().SetTarget(transform);
            if (!isPointsEarned)
            {
                GameManager.gm.earnPoints();
                isPointsEarned = true;
            }
            Destroy(gameObject, 5f);
        }
    }

    public void reduceHealth
        (int amount)
    {
        health -= amount;
    }
}
