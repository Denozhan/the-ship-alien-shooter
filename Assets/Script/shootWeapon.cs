using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shootWeapon : MonoBehaviour
{
    public int damage = 1;
    public Camera fpsCam;
    public AudioClip shootSFX;
    public ParticleSystem shootEffect;
    public ParticleSystem flareEffect;
    public float fireRate = 15f;

    private float _nextTimeToFire = 0f;
    // Update is called once per frame
    void Update()
    {
        if (Input.GetButton("Fire1") && Time.time >= _nextTimeToFire)
        {
            _nextTimeToFire = Time.time + 1f / fireRate;
            Shoot();
        }
    }

    void Shoot()
    {
        RaycastHit hit;
        if(Physics.Raycast(fpsCam.transform.position, fpsCam.transform.forward , out hit))
        {
            EnemyBehaviour enemy = hit.transform.GetComponent<EnemyBehaviour>();
            if (enemy != null)
            {
                enemy.reduceHealth(damage);
            }
            AudioSource.PlayClipAtPoint(shootSFX, fpsCam.transform.position);
            shootEffect.Play();
            ParticleSystem go = Instantiate(flareEffect, hit.point, Quaternion.LookRotation(hit.normal));
            go.Play();
            Destroy(go.gameObject,0.2f);
        }
    }
}
