﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FPCharacterController3D : MonoBehaviour
{
    public float moveSpeed = 12f;
    public Transform groundCheck;
    public float groundDistance = 0.4f;
    public LayerMask groundMask;
    public float gravity = -9.81f;
    public float jumpingHeight = 3f;
   

    private CharacterController _controller;
    private Vector3 _velocity;
    private bool _isGrounded;

    private void Start()
    {
        _controller = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {
        _isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);
            
        if(_isGrounded && _velocity.y<0) {
            _velocity.y = -2f;
        }
        
        if(_isGrounded && Input.GetButtonDown("Jump"))
        {
            _velocity.y = Mathf.Sqrt(jumpingHeight * -2 * gravity);
        }

        float vx = Input.GetAxis("Horizontal");
        float vz = Input.GetAxis("Vertical");

        Vector3 moveMotion = transform.right * vx + transform.forward * vz;

        _controller.Move(moveMotion * moveSpeed * Time.deltaTime);

        _velocity.y = _velocity.y + gravity * Time.deltaTime;

        _controller.Move(_velocity * Time.deltaTime);
    }
}
